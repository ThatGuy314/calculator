package edu.hawaii.ics211;

/**
 * Tests the Calculator class methods
 * 
 * @author Nagoshi, Vincent
 *
 */
public class TestCalculator {
  
  /**
   * Main program code
   * @param args, and array of Strings
   */
  public static void main(String[] args) {
    Calculator calc = new Calculator();
    
    //Testing add method.
    System.out.println("'2147483647 + 1': " + calc.add(Integer.MAX_VALUE, 1) + ": Did not account for exceeding maximum int value");
    
    //Testing subtract method.
    System.out.println("'-2147483648 - 1': " + calc.subtract(Integer.MIN_VALUE, 1) + ": Did not account for exceeding minimum int value");
    
    //Testing multiply method.
    System.out.println("'2147483647 * 2147483647': " + calc.multiply(Integer.MAX_VALUE, Integer.MAX_VALUE) + ": Did not account for exceeding minimum int value");
    
    //Testing divide method.
    try {
      calc.divide(100, 0);
    }
    catch (ArithmeticException e) {
      if(e.getLocalizedMessage().equals("/ by zero")) {
        System.out.println("'100 / 0': Did not account for dividing by 0");
      }
    }
    
    //Testing modulo method.
    try {
      calc.modulo(100, 0);
    }
    catch (ArithmeticException e) {
      if(e.getLocalizedMessage().equals("/ by zero")) {
        System.out.println("'100 % 0': Did not account for dividing by 0");
      }
    }
    
    //Testing pow method.
    System.out.println("'2 ^ 2147483647': " + calc.multiply(2, Integer.MAX_VALUE) + ": Did not account for exceeding minimum int value");
    
    System.out.println();
    System.out.println("Upper and lower caps for results should be added to prevent getting vastly incorrect returns do to the limitations of int values.");
    System.out.println("divide() and modulo() need to check if the second parameter is 0 to prevent '/ by zero' errors.");
  }
}
